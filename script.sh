#!/bin/bash

# Ejecutar script1.py
python 4.py &

# Esperar un momento para asegurarse de que el primer script se haya iniciado correctamente
sleep 1

# Ejecutar script2.py
python 5.py
