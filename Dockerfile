# Use an official Python runtime as a parent image
FROM python:3.9-slim

# Set the working directory in the container
WORKDIR /app

# Copy the Python scripts into the container at /app
COPY /app/4.py /app
COPY /app/5.py /app

# Copy the script that runs both Python scripts
COPY script.sh /app

# Make the script executable
RUN chmod +x script.sh

# Run the script that runs both Python scripts when the container launches
CMD ["./script.sh"]
