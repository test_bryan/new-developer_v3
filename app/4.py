
def main():
    """Run administrative tasks."""

    def find_matching_pair(numbers, target_sum):
        left = 0
        right = len(numbers) - 1

        while left < right:
            current_sum = numbers[left] + numbers[right]
            if current_sum == target_sum:
                return numbers[left], numbers[right]
            elif current_sum < target_sum:
                left += 1
            else:
                right -= 1

        return None

    # Example usage:
    numbers = [2, 3, 6, 7]
    target_sum = 9
    result = find_matching_pair(numbers, target_sum)
    if result:
        print(f"Matching pair: {result}")
    else:
        print("No matching pair found.")


if __name__ == '__main__':
    print("############## QUESTION 4 ################")
    main()
