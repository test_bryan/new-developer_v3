import unittest


def find_matching_pair(numbers, target_sum):
    left = 0
    right = len(numbers) - 1

    while left < right:
        current_sum = numbers[left] + numbers[right]
        if current_sum == target_sum:
            return numbers[left], numbers[right]
        elif current_sum < target_sum:
            left += 1
        else:
            right -= 1

    return None


class TestFindMatchingPair(unittest.TestCase):
    def test_matching_pair_found(self):
        numbers = [2, 3, 6, 7]
        target_sum = 9
        expected_result = (3, 6)
        self.assertEqual(find_matching_pair(numbers, target_sum), expected_result)

    def test_no_matching_pair(self):
        numbers = [1, 3, 3, 7]
        target_sum = 9
        self.assertIsNone(find_matching_pair(numbers, target_sum))


if __name__ == '__main__':
    unittest.main()
