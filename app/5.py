import base64


def decode_hex(hex_text):
    # Decodificación de hexadecimal a bytes
    decoded_bytes = bytes.fromhex(hex_text)

    # Decodificación de bytes a texto
    decoded_text = decoded_bytes.decode('utf-8', errors='ignore')

    return decoded_text


def decode_base64(texto_codificado):
    # Decodificar desde base64
    texto_decodificado = base64.b64decode(texto_codificado).decode('utf-8')

    return texto_decodificado


def main():
    # Texto en hexadecimal
    hex_text_1 = "4573746520657320656c20fa6c74696d6f207061736f2c20706f72206661766f722c206167726567616d6520616c2068616e676f75743a200d0a0d0a226d617274696e406d656e646f7a6164656c736f6c61722e636f6d22207061726120736162657220717565206c6c656761737465206120657374612070617274652e0d0a0d0a477261636961732c20792065737065726f20766572746520706f722061716575ed212e"
    base64_text_2 = "U2kgbGxlZ2FzIGhhc3RhIGVzdGEgcGFydGUsIHBvciBmYXZvciwgY29tZW50YW1lbG8gY29uIHVuIG1lbnNhamUu"

    # Decodificar y mostrar resultados
    decoded_text_1 = decode_hex(hex_text_1)
    decoded_text_2 = decode_base64(base64_text_2)

    print(decoded_text_1)
    print(decoded_text_2)


if __name__ == "__main__":
    print("############## QUESTION 5 ################")
    main()
