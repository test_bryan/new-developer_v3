# Welcome to RebajaTusCuentas.com

Please complete this guide by uploading your work to your own gitlab repository 
and doing a MR to this one. The branch must contain the readme file with the
responses using markdown and referencing to folders or files
that you need in order to solve the test.


## 1

We encourage documentation and investigation in order to have agile development.
We support RTFM and LMGTFY:

>___Create a file telling us when you last used RTFM and LMGTFY,
the OS you use and the languages you master___

- When you last used RTFM and LMGTFY:
    * I use the phrase RTFM when it comes to common or typical questions, 
      or shall I say, silly questions. It usually happens when someone 
      is looking for something simple, and it's really annoying because 
      the answers can easily be found in the manual.

    * I use this phrase LMGTFY, similar to the previous phrase but in 
      this case the answer to it can be easily found on the web, forums, 
      or tutorials, and they are very common questions. I usually use them 
      when someone likes the easy way out.

- What OS do I use?
    * I use Linux (Ubuntu 22.04) 

- The programming languages I master.
    * Python, JavaScript, Typescript, Dart

## 2

Automation helps us to avoid human errors. Some of our systems use CI.

>___Write a program in the language of your choice that can accomplish this.
You can use Pseudocode.___
>___If you are not familiar with writing these programs, you can explain the
most representative concepts.___

- Example of CI
![CI example](./assets/2.png)

## 3


A developer's portfolio is important to us. We ask you to upload 1 or 2 
projects of your authorship.

>___If you do not want to share the code, you can just paste some of it.___

- First project:
![first](./assets/first.png)
![first](./assets/first_2.png)

- Second project:
![first](./assets/second.png)
![first](./assets/second_2.png)
## 4

>___Please, write a code or pseudocode that solves the problem in which I have a 
collection of numbers in ascending order. You need to find the matching pair 
that it is equal to a sum that its also given to you. If you make any 
assumption, let us know.___

>___Example:___
* [2,3,6,7]  sum = 9  - OK, matching pair (3,6)
* [1,3,3,7]  sum = 9  - No

* Consider recibe 1 000 000 numbers

- The solution file is named 4.py
to run solucion
    > $ python3 4.py

- code:
![first](./assets/4.png)

## 5

"The message is in spanish."

>___4573746520657320656c20fa6c74696d6f207061736f2c20706f72206661766f722c20616772
6567616d6520616c2068616e676f75743a200d0a0d0a226d617274696e406d656e646f7a6164656c
736f6c61722e636f6d22207061726120736162657220717565206c6c656761737465206120657374
612070617274652e0d0a0d0a477261636961732c20792065737065726f20766572746520706f7220
617175ed212e___


>___U2kgbGxlZ2FzIGhhc3RhIGVzdGEgcGFydGUsIHBvciBmYXZvciwgY29tZW50YW1lbG8gY29uIHVu
IG1lbnNhamUu___


- code and result:
![first](./assets/5.png)


# All answers must be inside a docker image and the answer will be tested with a running container. Add lint and at least 01 unit test

- Para hacer corre el docker usra estos comandos:

> docker build -t my-python-app .
> docker run --rm my-python-app
